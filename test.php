<!doctype html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <title>The HTML5 Herald</title>
    <meta name="description" content="The HTML5 Herald">
    <meta name="author" content="SitePoint">
    <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAURh6MlIqw2xDD1Migdd9JivJ5MSFknt0&callback=initMap"  async defer></script>
    <?php  if (($handle = fopen('tracks/100.csv', 'r')) !== FALSE) { 
             $i = 0;
             while (($data[$i] = fgetcsv($handle, 1000, ',')) !== FALSE) {
                 $i++;
             }
            fclose($handle);
        } 
        else 
          $data = "fisier bulit";
        ?>
    <script >
    function initMap() {
        var ltlng = [];
        <?php for($j=1;$j<$i;$j++) print("ltlng.push(new google.maps.LatLng(". $data[$j][0] . "," . $data[$j][1] ."));\n") ?>   
       
        var myOptions = {
            zoom: 18,
            //center: latlng,
            center: ltlng[0],
            mapTypeId: google.maps.MapTypeId.ROADMAP
        };
        var map = new google.maps.Map(document.getElementById("map"), myOptions);
        /* Marker
        for (var i = 0; i < ltlng.length; i++) {
            var marker = new google.maps.Marker
                (
                {
                    // position: new google.maps.LatLng(-34.397, 150.644),
                    position: ltlng[i],
                    map: map,
                    title: 'Click me'
                }
                );
        }
        */
        //***********ROUTING****************//
        //Intialize the Path Array
        var path = new google.maps.MVCArray();
        //Intialize the Direction Service
        var service = new google.maps.DirectionsService();
        var flightPath = new google.maps.Polyline({
        path: ltlng,
        geodesic: true,
        strokeColor: '#A91627',
        strokeOpacity: 1.0,
        strokeWeight: 2
    });

    flightPath.setMap(map);

    }
    window.onload = initMap;
</script>
</head>
<html>
<body>
    <h2>Creating Your First Google Map Demo:</h2>
    
    <div id="map" style="width: 1200px; top: 68px; left: 172px; position: absolute; height: 800px">
</div>
</body>
</html>