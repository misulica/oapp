<html lang="en">
<head>
  <meta charset="utf-8">

  <title>O-App</title>
  <meta name="description" content="Aplicatie pentru poligoane fixe de orientare ">
  <meta name="author" content="Mihai Santa">
  <link rel="stylesheet" href="style.css">
  <Script>
      function afiseazaTop10(hIde) { 
  if (document.getElementById(hIde).style.display === "block") {
    //document.getElementById(hIde).style.display = "none";
    document.getElementById(hIde).classList.toggle('change');
  } else {
    //document.getElementById(hIde).style.display = "block";
    document.getElementById(hIde).classList.toggle('change');
  }
}
    function openStart(id_track){
        window.location("https://www.w3schools.com");
               window.location.assign(("start.php?track_id="+id_track));
}
</script>
</head>

<body>
<?php include "header.php"; ?>
<div class="content">
    <?php
    ini_set('display_errors', 1);
    ini_set('display_startup_errors', 1);
    //error_reporting(E_ALL);
    include "config.php";
    $zona = $_GET['zone'];
    $link = mysqli_connect($server, $user, $pass, $dba);
            if (!$link) {
                echo "Error: Unable to connect to MySQL." . PHP_EOL;
                echo "Debugging errno: " . mysqli_connect_errno() . PHP_EOL;
                echo "Debugging error: " . mysqli_connect_error() . PHP_EOL;
                exit;
            }
    mysqli_set_charset($link, "utf8");
    $sql = "SELECT * FROM zone WHERE id_zone ='". $zona ."'";
    $result = mysqli_query($link, $sql);
    $row = mysqli_fetch_assoc($result);
    echo "<h1 class='titluAlb'>". $row['nume'] ."</h1>";
    echo "<h4><a href='". $row['allcontrol'] ."' target='_blank'>all control</a></h4>";
    $sql = "SELECT * FROM tracks WHERE id_zone ='". $zona ."'";
    $result2 = mysqli_query($link, $sql);
    for($i=0; $row2 = mysqli_fetch_assoc($result2); $i++)
    {
    ?>
    <div class="traseu">
    <div class="numeDescarca">
        <div class="child" id="nume"><h2><?php echo $row2['nume']; ?></h2></div>
        <div class="child" id="descarca"><a href='harti/<?php echo $row2['fisier'];?>' download><img src="img/download.png"></a></div>
        <div class="child" id="mail"><a href='mailto:?subject=Harta <?php echo " " .$row['nume'] . " " .$row2['nume'] ;?>&body=De aici poti descarca harta: https://csbabarunca.ro/oapp/harti/<?php  echo $row2['fisier'];?> '><img src="img/email.png"></a></div>
        <div class="child" id="play"><a href='start.php?track_id=<?php echo $row2['id_track'];?>'><img src="img/play.png" ></a></div>
    </div>
     
    <div class="descriere">
         <p class="whiteText"><?php echo $row2['descriere']; ?></p>
        </div>
    <div class="stats">
            <div>
            <?php //find the record for this course
            //get track_id
            $sql = "SELECT *  FROM run WHERE id_track ='". $row2['id_track'] ."' AND end_time > 0 ORDER BY (end_time - start_time) ASC";
            //echo $sql;
            $result3 = mysqli_query($link, $sql);
            $row3 = mysqli_fetch_assoc($result3);
            ?>
            <img id="times" src="img/times.png">
            <p class="whiteText"><?php echo mysqli_affected_rows ($link); ?>x <br> Alergat</p>
            </div>
            <div>
            <img src="img/medal.png" id="medal">
           
            <p class="whiteText"><?php echo $row3['nickname']; ?><br> <?php echo sprintf("%0d:%0d secunde",intval(($row3['end_time']-$row3['start_time'])/60),($row3['end_time']-$row3['start_time'])%60); ?> </p>
            </div>
            <div>
            <img id="semnTop10" onclick="afiseazaTop10('ht<?php echo $i;?>',10);" src="img/top10.png">
            <p class="whiteText clasament" >Clasament<br>General</p>
            </div>
        </div>  
      
        <div class="top10" id="ht<?php echo $i;?>">
            <h2 style="color: #FFA500;">Top 10 în ultimele 90 de zile</h2>  
            <?php 
            for ($i=0;$i<mysqli_affected_rows ($link) AND $i<10;$i++)//for each row print a new row max 10
                {
                    
                    //calculate date 
                    //var_dump($row3);
                    $date = date_create();
                    date_timestamp_set($date, (int)$row3['start_time']);
                    if(intval($row3['end_time'])>0)
                    {
                     $timpul = sprintf("%02d:%0d",intval(($row3['end_time']-$row3['start_time'])/60),($row3['end_time']-$row3['start_time'])%60);
                     echo "<div class='WhiteTextSm'>". ($i+1) .". ". sprintf("%10s", $row3['nickname']) ." - " . date_format($date, 'Y-m-d') . " - " . $timpul . "</div>";
                     
                    }
                    $row3 = mysqli_fetch_assoc($result3);
                }      
            ?>
            
        </div>
    </div>    
        <?php
            
    }
    ?>
    <?php include "footer.php"; ?>
    </div>
</div>
</body>
</html>