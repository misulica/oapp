
<html>
<head>
<link rel="stylesheet" href="style.css"> 
<script type="text/javascript" src="js/html2canvas.js"></script>

</head>
<body id="finishPop">
<?php
if (!isset($_GET["Run_id"])){
   echo "Cant see Run ID :((";
} else {
    $runID = $_GET["Run_id"];
    
    //echo "Run ID : " . $_GET["Run_id"] ."<br>";
    include("config.php");
    $link = mysqli_connect($server, $user, $pass, $dba);
    if (!$link) {
        echo "Error: Unable to connect to MySQL." . PHP_EOL;
        echo "Debugging errno: " . mysqli_connect_errno() . PHP_EOL;
        echo "Debugging error: " . mysqli_connect_error() . PHP_EOL;
        exit;
    }
    $sql = "SELECT * FROM run WHERE id_run=". $runID  ;
    //echo $sql;
    $result = mysqli_query($link, $sql);
    $row = mysqli_fetch_assoc($result);
    //var_dump($row);
    $time = (doubleval($row['end_time']) - doubleval($row['start_time']));
    // get track, get zone
    //echo var_dump($row);
    $track_id = $row['id_track'];
    $sql = "SELECT * FROM tracks WHERE id_track=". $track_id  ;
    $result2 = mysqli_query($link, $sql);
    $row2 = mysqli_fetch_assoc($result2);
    $zona = $row2['id_zone'];
    $sql = "SELECT * FROM zone WHERE id_zone=". $zona  ;
    $result3 = mysqli_query($link, $sql);
    $row3 = mysqli_fetch_assoc($result3);
    //echo var_dump($row2);

    echo "<h1>Felicitări!!!</h1><h2>". $row['nickname'] ."</h2><h3 class='portocaliu'>". $row3['nume'] ." - ". $row2['nume'] ."</h3><h3>Ai terminat în:</h3><h2> ". sprintf("%02d:%02d:%02d",intval($time/3600),intval($time-intval($time/3600)*3600)/60, $time%60) ."</h2>";
    //Splituri
    // cautam dupa Run_iD in run_cp, crescator dupa id_CP si afisam secunde, secunde 
    $sql = "SELECT * FROM run_cp WHERE id_run_id=". $runID ." ORDER BY id_cp ASC" ;
    //echo $sql;
    $result = mysqli_query($link, $sql);
    echo "<div class='tabel'><table><tr><th>CP</th><th>Timp</th>";
    $aux = 0;
    while ($row2 = mysqli_fetch_assoc($result)){
        //var_dump($row2);
        if($aux++ == 0)
            $last = 0;
        echo "<tr><td>". $row2['id_cp'] ."</td><td>". intval(($row2['timestamp']-$last)/60) .":". sprintf("%02d", ($row2['timestamp']-$last)%60) ."</td></tr>";
        $last = $row2['timestamp'];
    } 
    echo "</table></div>";
    //Leaderboard
    //echo "<h3>Momentan ești pe locul 1 până implementez un clasament general!<h3>";
    echo "<div id='site' style='display:none;'><h4>www.csbabarunca.ro/oapp</h4></div>";
    echo "<div class='tabel' id='ascunde'><a class='element' id='capture'>Salvează!</a><br><br>";
    echo "<a class='element' id='meniu' href='index.php'> Meniu</a></div>";
}
?>
</body>
</html>
<script type="text/javascript">
 const capture = () => {
    document.getElementById('capture').style.display = "none";
    document.getElementById('meniu').style.display = "none";
    document.getElementById('site').style.display = "inline";
  const body = document.querySelector('body');
  body.id = 'capture';
  html2canvas(document.querySelector("#capture")).then(canvas => {
    document.body.appendChild(canvas);
  }).then(() => {
    var canvas = document.querySelector('canvas');
    canvas.style.display = 'none';
    var image = canvas.toDataURL("image/png").replace("image/png", "image/octet-stream");
    var a = document.createElement("a");
    a.setAttribute('download', 'run<?php echo $runID;?>.png');
    a.setAttribute('href', image);
    a.click();
  });
  document.getElementById('site').style.display = "none";
  document.getElementById('meniu').style.display = "inline";
};

const btn = document.getElementById('capture');
btn.addEventListener('click', capture)
</script>
