<html lang="en">
<head>
  <meta charset="utf-8">

  <title>O-App</title>
  <meta name="description" content="Aplicatie pentru poligoane fixe de orientare ">
  <meta name="author" content="Mihai Santa">
  <link rel="stylesheet" href="style.css">
  <script>
  //calculeaza distanta intre doua puncte
  function calcDist(lat1, lon1, lat2, lon2) 
    {
      var R = 6371; // km
      var dLat = toRad(lat2-lat1);
      var dLon = toRad(lon2-lon1);
      var lat1 = toRad(lat1);
      var lat2 = toRad(lat2);

      var a = Math.sin(dLat/2) * Math.sin(dLat/2) +
        Math.sin(dLon/2) * Math.sin(dLon/2) * Math.cos(lat1) * Math.cos(lat2); 
      var c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1-a)); 
      var d = R * c;
      return Number(d).toFixed(2);
    }

    // Converts numeric degrees to radians
function toRad(Value) 
    {
        return Value * Math.PI / 180;
    }
//ask for geolocation
var loc; 
var options = {
  enableHighAccuracy: true,
  timeout: 5000,
  maximumAge: 0
};
function success(pos) {
  var crd = pos.coords;
  document.getElementById("demo").innerHTML = "Locatia mea:<br>Lat: " + Number(pos.coords.latitude).toFixed(6) + 
  "<br>Long: " + Number(pos.coords.longitude).toFixed(6);
  console.log('Your current position is:');
  console.log(`Latitude : ${crd.latitude}`);
  console.log(`Longitude: ${crd.longitude}`);
  console.log(`More or less ${crd.accuracy} meters.`);
  //ia frumos toate getLat, getLong si calculeaza distanta
  var x = document.getElementsByClassName("getLat");
  var y = document.getElementsByClassName("getLong");
  var dist = document.getElementsByClassName("setDist");
  console.log(dist);
  var z = document.getElementById("harti");
  for ( i=0; i< x.length; i++){
    console.log(x[i].innerHTML +'->'+ y[i].innerHTML +'->'+ crd.latitude +'->'+crd.longitude);
    z.getElementsByClassName("setDist")[i].innerHTML = "Distanta: "+ calcDist(x[i].innerHTML,y[i].innerHTML,crd.latitude,crd.longitude)+" Km";
  }
  
}
function error(err) {
  document.getElementById("demo").innerHTML = err.message;
}
function afiseazaHartile() {
  document.getElementById("harti").style.display = "flex"; 
  navigator.geolocation.getCurrentPosition(success, error, options);
}
</script>
<script> //Anchor jump care tine cont de grosimea header-ului
window.addEventListener("hashchange", function () {
    window.scrollTo(window.scrollX, window.scrollY - 120);
});
</script>
</head>

<body>
<?php
include "header.php";
?>
<div class="item">
    <div id="divDescriere">
        <h1>Descriere</h1>        
        <p>Dacă tot ne-am apucat de construit poligoane fixe de orientare hai să venim și cu câteva provocări: <br>
         - Alege unul din trasee pregătite și vezi în cât timp poți să-l parcurgi<br>
         - Folosește harta generală și concepe un traseu pe care ni-l trimiți iar noi îl adaugăm în aplicație<br>
         - Fii creativ și provoacă-ți prietenii !
        </p>
    </div>
    <img class="poze" id="pozaDescriere" src="img/pic1.jpg" alt="Poza aplicatie in teren">
      
    </div>
<div class="item">
    <img class="poze" id="pozaHowTo" src="img/pic2.jpg" alt="Poza harta in teren">
    <div id="divHowTo">
        <h1>Cum ne jucăm?</h1>        
        <p>Cauți o zonă de joacă din cele de mai jos, și îți alegi harta pe care vrei să te deplasezi. O printezi și dacă vrei să fie mai rezistentă o poți pune într-o țiplă. Cu ea și o busolă te deplasezi la zona de Start, postul K de pe hartă. Intri de pe telefon pe site pe aceeași hartă și dai Start. De aici începe aventura, parcurgând post cu post traseul desenat și validând posturile din teren cu cele din aplicație. La final vei vedea timpul obiținut și locul în clasamentul general. Pentru detalii pas cu pas poți accesa acest  <a href="howto/howToOapp.pdf"> mic ghid. </a> </p>
    </div>
    
    
</div>
<div class="block" >
    <a class="start" id="bHarti" onclick="afiseazaHartile()"  >Afișează Zonele</a> 
    <h4 id="demo">Locatia mea<h4>
</div>
    <div class="harti" id="harti" >    
        <!-- aici merge un for si afisam toate zonele din baza de date -->
        <?php
        include "config.php";
        $link = mysqli_connect($server, $user, $pass, $dba);
        if (!$link) {
            echo "Error: Unable to connect to MySQL." . PHP_EOL;
            echo "Debugging errno: " . mysqli_connect_errno() . PHP_EOL;
            echo "Debugging error: " . mysqli_connect_error() . PHP_EOL;
            exit;
        }
        mysqli_set_charset($link, "utf8");
        $sql = "SELECT * FROM zone";
        $result = mysqli_query($link, $sql);
        while($row = mysqli_fetch_assoc($result))
        {
        ?>
        <div class="oZona">  
            <a href="map.php?zone=<?php echo $row['id_zone']; ?>">          
            <img class="under" src="<?php echo $row['poza1']; ?>" alt="Poza fundal lipsa">
            <img class="over" src="<?php echo $row['poza2']; ?>"  alt="Poza harta lipsa">   
            <h3><?php echo $row['nume']; ?></a></h3>
              <?php
                $sql2 = "SELECT * FROM tracks WHERE id_zone ='". $row['id_zone'] ."'";
                //echo $sql2;
                $result2 = mysqli_query($link, $sql2);
              ?>
            <h4>Trasee: <?php echo  mysqli_affected_rows($link) ?></h4>
            <p class="getLat"><?php echo  $row['latitude']; ?></p>
            <p class="getLong"><?php echo  $row['longitude']; ?></p>
            <h4 class="setDist">Apropiere: ?km</h4>
            <h4><a target="_blank" href="http://www.google.com/maps/place/<?php echo $row['latitude'] .','. $row['longitude'] ;?>">Cum ajung?</a></h4>
        </div>
        <?php } ?>
    </div>
<?php
include "footer.php";
?>
</body>
</html>