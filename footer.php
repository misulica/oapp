<footer>
    <div>
       <h3>Intrebări frecvente</h3>
       <p class="lime">Nu am imprimantă? Pot lua harțile din altă parte ?</p>
       <p>- Sigur, poți să le imprimi la Glumy</p>
       <p class="lime">Cum știu exact de unde să iau startul?</p>
       <p>- Identifică locația postului K, este desenat cu un triunghi mov</p>
       <p class="lime">L-am găsit pe hartă dar nu-l văd în teren</p>
       <p>- Este fictiv, îți indică zona recomandată de unde să începi</p>
    </div>
    <div id="contact">
        <h1>Contact</h1>
        <h4>Misha<br> 
        mihai.santa (at) gmail.com</h4>
    </div>

</footer>  