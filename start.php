<html>
<head>
<link rel="stylesheet" href="style.css"> 
<script>
  //calculeaza distanta intre doua puncte
  function calcDist(lat1, lon1, lat2, lon2) 
    {
      var R = 6371; // km
      var dLat = toRad(lat2-lat1);
      var dLon = toRad(lon2-lon1);
      var lat1 = toRad(lat1);
      var lat2 = toRad(lat2);

      var a = Math.sin(dLat/2) * Math.sin(dLat/2) +
        Math.sin(dLon/2) * Math.sin(dLon/2) * Math.cos(lat1) * Math.cos(lat2); 
      var c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1-a)); 
      var d = R * c;
      return Number(d).toFixed(2);
    }

    // Converts numeric degrees to radians
function toRad(Value) {
    return Value * Math.PI / 180;
}
//ask for geolocation
var loc; 
var options = {
  enableHighAccuracy: true,
  timeout: 5000,
  maximumAge: 0
};
function success(pos) {
  var crd = pos.coords;
  /* document.getElementById("demo").innerHTML = "Locatia mea:<br>Lat: " + Number(pos.coords.latitude).toFixed(6) + 
  "<br>Long: " + Number(pos.coords.longitude).toFixed(6); */
  console.log('Your current position is:');
  console.log(`Latitude : ${crd.latitude}`);
  console.log(`Longitude: ${crd.longitude}`);
  console.log(`More or less ${crd.accuracy} meters.`);
  //ia frumos toate getLat, getLong si calculeaza distanta
  var x = document.getElementsByClassName("getLat");
  var y = document.getElementsByClassName("getLong");
  var dist = document.getElementsByClassName("setDist");
  console.log(x[0].innerHTML);
  console.log(y[0].innerHTML);
  console.log(dist[0]);
  dist[0].innerHTML = "Distanța: "+ calcDist(x[0].innerHTML,y[0].innerHTML,crd.latitude,crd.longitude)*1000+" m";
}
function error(err) {
  document.getElementsByClassName("setDist")[0].innerHTML = err.message;
}
function verifica() {
   navigator.geolocation.getCurrentPosition(success, error, options);
}
</script>
</head>
<body>
<?php 
 include "header.php";
 include "config.php";
 $trackId = $_GET['track_id'];
 $link = mysqli_connect($server, $user, $pass, $dba);
 if (!$link) {
     echo "Error: Unable to connect to MySQL." . PHP_EOL;
     echo "Debugging errno: " . mysqli_connect_errno() . PHP_EOL;
     echo "Debugging error: " . mysqli_connect_error() . PHP_EOL;
     exit;
 }
mysqli_set_charset($link, "utf8");
$sql = "SELECT * FROM tracks WHERE id_track ='". $trackId ."'";
$result = mysqli_query($link, $sql);
$row = mysqli_fetch_assoc($result);
?>
<div class="content">
    <?php echo "<h1>". $row['nume'] ."</h1>"; ?>    
    <h3>1. Mergi la zona de start:<a href="http://www.google.com/maps/place/<?php echo $row['startlat'] .','. $row['startlong'] ;?>" target="_blank"><img id="pin" src="img/pin.png"></a></h3>
    <div class="whiteText setDist" id="demo">Distanța până la start:</div>
    <p class="getLat"><?php echo  $row['startlat']; ?></p>
    <p class="getLong"><?php echo  $row['startlong']; ?></p>           
    <div class="verifica" onclick='verifica()'>Verifică</div>
    <h3>2. Alege-ți un nume</h3>
    <form action="run.php" method="get"> 
    <?php include "message.php"; ?>
    <div class= "nick"><input  type="text" name="Nickname" placeholder="<?php echo $nickMes[random_int(0,$nickCate-1)];?>" value=""></div>
    <input type="text" name="Track_ID" style="display: none;" value="<?php echo $trackId; ?>"> <!-- will be hidden -->
    <h3>3. Când ești pregătit apasă </h3> 
    <div class="startBT"><input class="start" type="submit" value="START"></div>
    </form>
</div>
</body>
</html>