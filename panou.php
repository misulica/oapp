<html lang="en">
<title>O-App - Panou</title>
  <meta name="description" content="Aplicatie pentru poligoane fixe de orientare ">
  <meta name="author" content="Mihai Santa">
  <link rel="stylesheet" href="style.css">
<head>
</head>
<body>
<h1>Panou administrare</h1>
<div class='flex'>
<form class='adauga' action="addZone.php"><input class="nick mare" type="text" name="numeZona" placeholder="Nume zona noua"><input class="start mic" type="submit" value="Adaugă"></form>
</div>
<div>
  <h3><a class="bt_verde" href="showRuns.php">Show/Clean Runs</a></h3>
</div>
<?php 
  include "config.php";
  $link = mysqli_connect($server, $user, $pass, $dba);
  if (!$link) {
      echo "Error: Unable to connect to MySQL." . PHP_EOL;
      echo "Debugging errno: " . mysqli_connect_errno() . PHP_EOL;
      echo "Debugging error: " . mysqli_connect_error() . PHP_EOL;
      exit;
  }
  mysqli_set_charset($link, "utf8");
  $sql = "SELECT * FROM zone ORDER BY nume ASC";
  $result = mysqli_query($link, $sql);
  while($row = mysqli_fetch_assoc($result)){
  //for each zone
    echo "<div  class='adauga' ><h2>". $row['nume'] ."</h2>";
    echo "<form class='adauga' style='display:flex;' action='addTrack.php'><input class='nick mare' type='text' placeholder='Nume Track Nou'><input class='start mic' type='submit' value='Adaugă'></form></div>";
    // for each already track
    $sql = "SELECT * FROM tracks WHERE id_zone='". $row['id_zone'] ."'";
    $result2 = mysqli_query($link, $sql);
    while($row2 = mysqli_fetch_assoc($result2)){
      echo "<div class='adauga'  '>";
      echo "<h3 style='width:40%;'>" . $row2['nume'] . "</h3>";
      echo "<a class='bt_verde' href='editTrack.php?trackId=" .""."' target='_blank'>edit</a>"; 
      echo "<a class='bt_verde' href='showRuns.php?trackId=" .""."' target='_blank'>Runs</a>";
      echo "<a class='delete' href='deleteTrack.php?trackId=" .""."' target='_blank'>D</a>";  
      echo "</div>";
    }
  }
?>
</body>
</htm>