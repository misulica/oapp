-- phpMyAdmin SQL Dump
-- version 5.0.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Apr 05, 2020 at 10:33 PM
-- Server version: 5.7.24-log
-- PHP Version: 7.4.3

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `oapp`
--

-- --------------------------------------------------------

--
-- Table structure for table `run`
--

CREATE TABLE `run` (
  `id_run` int(11) NOT NULL,
  `id_track` int(11) DEFAULT NULL,
  `nickname` varchar(45) COLLATE utf8_bin DEFAULT NULL,
  `start_time` varchar(45) COLLATE utf8_bin DEFAULT NULL,
  `end_time` varchar(45) COLLATE utf8_bin DEFAULT NULL,
  `ok` varchar(45) COLLATE utf8_bin DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Dumping data for table `run`
--

INSERT INTO `run` (`id_run`, `id_track`, `nickname`, `start_time`, `end_time`, `ok`) VALUES
(78, 1, '   Johny Jacobs', '1584999633', '1584999648', NULL),
(79, 1, 'Msiha', '1585002287', '1585002352', NULL),
(80, 1, '   Johny Jacobs', '1585002462', '1585002475', NULL),
(81, 1, '   Johny Jacobs', '1585041814', '1585041852', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `run_cp`
--

CREATE TABLE `run_cp` (
  `id_run_cp` int(11) NOT NULL,
  `id_run_id` int(11) NOT NULL,
  `id_cp` int(11) DEFAULT NULL,
  `timestamp` varchar(45) COLLATE utf8_bin DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin COMMENT='	';

--
-- Dumping data for table `run_cp`
--

INSERT INTO `run_cp` (`id_run_cp`, `id_run_id`, `id_cp`, `timestamp`) VALUES
(3, 77, 3, '6'),
(4, 77, 4, '25'),
(5, 77, 5, '53'),
(6, 77, 6, '55'),
(7, 77, 7, '57'),
(8, 78, 1, '2'),
(9, 78, 2, '5'),
(10, 78, 3, '8'),
(11, 78, 4, '13'),
(12, 78, 5, '15'),
(13, 74, 1, '3650'),
(14, 74, 2, '3652'),
(15, 74, 3, '3654'),
(16, 74, 4, '3657'),
(17, 74, 5, '3658'),
(18, 79, 1, '6'),
(19, 79, 2, '21'),
(20, 79, 3, '26'),
(21, 79, 4, '35'),
(22, 79, 5, '65'),
(23, 80, 1, '2'),
(24, 80, 2, '5'),
(25, 80, 3, '7'),
(26, 80, 4, '9'),
(27, 80, 5, '13'),
(28, 81, 1, '15'),
(29, 81, 2, '26'),
(30, 81, 3, '30'),
(31, 81, 4, '35'),
(32, 81, 5, '38');

-- --------------------------------------------------------

--
-- Table structure for table `tracks`
--

CREATE TABLE `tracks` (
  `id_track` int(11) NOT NULL,
  `id_zone` int(11) NOT NULL,
  `nume` varchar(45) COLLATE utf8_bin DEFAULT NULL,
  `descriere` text COLLATE utf8_bin,
  `startlong` varchar(15) COLLATE utf8_bin NOT NULL,
  `startlat` varchar(15) COLLATE utf8_bin NOT NULL,
  `finishlong` varchar(15) COLLATE utf8_bin NOT NULL,
  `finishlat` varchar(15) COLLATE utf8_bin NOT NULL,
  `nr_cpuri` int(11) DEFAULT NULL,
  `fisier` varchar(45) COLLATE utf8_bin NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin COMMENT='Traseele	';

--
-- Dumping data for table `tracks`
--

INSERT INTO `tracks` (`id_track`, `id_zone`, `nume`, `descriere`, `startlong`, `startlat`, `finishlong`, `finishlat`, `nr_cpuri`, `fisier`) VALUES
(1, 1, 'Galbem', '2000m / 70+m \r\nUsor, durata aproximativa 30 minute', '25.7050300000', '45.6091900000', '25.7050300000', '45.6091900000', 5, ''),
(2, 1, 'Rosu', '3000m / 250+m\r\nMediu, durata aproximativa 60 minute', '25.7050300000', '45.6091900000', '25.7050300000', '45.6091900000', 7, '');

-- --------------------------------------------------------

--
-- Table structure for table `track_cp`
--

CREATE TABLE `track_cp` (
  `id_track_cp` int(11) NOT NULL,
  `id_track` int(11) DEFAULT NULL,
  `order_id` varchar(11) COLLATE utf8_bin NOT NULL,
  `cp_number` varchar(45) COLLATE utf8_bin DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Dumping data for table `track_cp`
--

INSERT INTO `track_cp` (`id_track_cp`, `id_track`, `order_id`, `cp_number`) VALUES
(1, 1, '1', '30'),
(2, 1, '2', '34'),
(3, 1, '3', '42'),
(4, 1, '4', '37'),
(5, 1, '5', '32');

-- --------------------------------------------------------

--
-- Table structure for table `zone`
--

CREATE TABLE `zone` (
  `id_zone` int(11) NOT NULL,
  `nume` varchar(25) COLLATE utf8_bin NOT NULL,
  `poza1` varchar(45) COLLATE utf8_bin NOT NULL,
  `poza2` varchar(45) COLLATE utf8_bin NOT NULL,
  `latitude` decimal(13,10) NOT NULL,
  `longitude` decimal(13,10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Dumping data for table `zone`
--

INSERT INTO `zone` (`id_zone`, `nume`, `poza1`, `poza2`, `latitude`, `longitude`) VALUES
(1, 'Highis', 'zone/highis1.jpg', 'zone/highis2.jpg', '45.6091900000', '25.7050300000'),
(2, 'Noua', 'zone/noua1.jpg', 'zone/noua2.jpg', '45.6143200000', '25.6404700000'),
(3, 'Test', 'zone/test1.jpg', 'zone/test2.jpg', '46.0000000000', '26.0000000000');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `run`
--
ALTER TABLE `run`
  ADD PRIMARY KEY (`id_run`);

--
-- Indexes for table `run_cp`
--
ALTER TABLE `run_cp`
  ADD PRIMARY KEY (`id_run_cp`);

--
-- Indexes for table `tracks`
--
ALTER TABLE `tracks`
  ADD PRIMARY KEY (`id_track`),
  ADD UNIQUE KEY `id_track_UNIQUE` (`id_track`);

--
-- Indexes for table `track_cp`
--
ALTER TABLE `track_cp`
  ADD PRIMARY KEY (`id_track_cp`),
  ADD UNIQUE KEY `id_track_cp_UNIQUE` (`id_track_cp`);

--
-- Indexes for table `zone`
--
ALTER TABLE `zone`
  ADD PRIMARY KEY (`id_zone`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `run`
--
ALTER TABLE `run`
  MODIFY `id_run` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=82;

--
-- AUTO_INCREMENT for table `run_cp`
--
ALTER TABLE `run_cp`
  MODIFY `id_run_cp` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=33;

--
-- AUTO_INCREMENT for table `tracks`
--
ALTER TABLE `tracks`
  MODIFY `id_track` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `zone`
--
ALTER TABLE `zone`
  MODIFY `id_zone` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
